﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DriveAPI
{
    class Program
    {
        static void Main(string[] args)
        {


            var folder = DriverOperations.CreateFolder("Alictus");
            DriverOperations.UploadFile(@"C:\git_practice\DriveAPI\DriveAPI\bin\Debug\Alictus.csv", folder);

            DriverOperations.AddTotalBudget();

        }

      
    }
}