﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using File = Google.Apis.Drive.v3.Data.File;

namespace DriveAPI
{
    internal static class DriverOperations
    {
        // If modifying these scopes, delete your previously saved credentials
        // at ~/
        static string[] Scopes = { DriveService.Scope.Drive, SheetsService.Scope.Spreadsheets };
        static string ApplicationName = "Drive-example";

        internal static File CreateFolder(string folderName)
        {
            var service = GetDriverService();
            var fileMetadata = new File()
            {
                Name = folderName,
                MimeType = "application/vnd.google-apps.folder"
            };

            foreach (var fil in GetFileList())
            {
                if (fil == fileMetadata)
                {
                    return fil;
                }
            }

            var request = service.Files.Create(fileMetadata);
            request.Fields = "id";
            var file = request.Execute();
            Console.WriteLine("Folder ID: " + file.Id);

            return file;

        }
        internal static IList<File> GetFileList()
        {
            var service = GetDriverService();
            // Define parameters of request.
            FilesResource.ListRequest listRequest = service.Files.List();

            listRequest.PageSize = 100;
            listRequest.Fields = "nextPageToken, files(id, name)";

            // List files.
            IList<Google.Apis.Drive.v3.Data.File> files = listRequest.Execute()
                .Files;

            Console.WriteLine("Files:");

            return files;
        }

        internal static void UploadFile(string filePath, File parent)
        {
            var service = GetDriverService();

            File body = new File();

            body.Name = System.IO.Path.GetFileName(filePath);
            body.MimeType = "application/vnd.google-apps.spreadsheet";
            body.Parents = new List<string> { parent.Id };
            byte[] byteArray = System.IO.File.ReadAllBytes(filePath);
            System.IO.MemoryStream stream = new System.IO.MemoryStream(byteArray);

            FilesResource.CreateMediaUpload request = service.Files.Create(body, stream, "text/csv");

            request.SupportsTeamDrives = true;
            request.Upload();

            Console.WriteLine("File ID: " + body.Id);
        }

        internal static void UpdateFile(string FileId, File parent)
        {
            var service = GetDriverService();

            ////FilesResource.CreateMediaUpload request = service.Files.Update(body, stream, "application / vnd.google - apps.spreadsheet");
            //request.SupportsTeamDrives = true;
            //request.Upload();
        }

        internal static void AddTotalBudget()
        {
            try
            {
                var lstFiles = GetFileList();
                String range = "A1:F1";
                foreach (var file in lstFiles)
                {
                    SpreadsheetsResource.ValuesResource.GetRequest request =
                   GetSheetsService().Spreadsheets.Values.Get(file.Id, range);

                    ValueRange response = request.Execute();
                    IList<IList<Object>> values = response.Values;
                    if (values != null && values.Count > 0)
                    {
                        foreach (var value in values)
                        {
                            value.Add("Total Budget");
                        }
                    }

                    SpreadsheetsResource.ValuesResource.GetRequest requestBody =
                  GetSheetsService().Spreadsheets.Values.Get(file.Id, "B2:F14");

                    ValueRange responseBody = requestBody.Execute();
                    IList<IList<Object>> valuesBody = responseBody.Values;
                    if (valuesBody != null && valuesBody.Count > 0)
                    {
                        foreach (var value in valuesBody)
                        {
                            value.Add(double.Parse(value[2].ToString()) * double.Parse(value[4].ToString()));
                        }
                    }

                    ValueRange valueRange = new ValueRange();
                    valueRange.Values = values;
                    valueRange.Range = "A1:G1";
                    SpreadsheetsResource.ValuesResource.UpdateRequest update =
                GetSheetsService().Spreadsheets.Values.Update(valueRange, file.Id, "A1:G1");
                    update.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.RAW;
                    update.ExecuteAsync();

                    ValueRange valueRangeBody = new ValueRange();
                    valueRangeBody.Values = valuesBody;
                    valueRangeBody.Range = "B2:G14";
                    SpreadsheetsResource.ValuesResource.UpdateRequest updateBody =
                GetSheetsService().Spreadsheets.Values.Update(valueRangeBody, file.Id, "B2:G14");
                    updateBody.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.RAW;
                    updateBody.ExecuteAsync();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ex");
            }
        }

        internal static SheetsService GetSheetsService()
        {
            UserCredential credential;

            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = GetCredential(),
                ApplicationName = ApplicationName,
            });

            return service;
        }

        internal static DriveService GetDriverService()
        {
            // Create Drive API service.
            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = GetCredential(),
                ApplicationName = ApplicationName,
            });

            return service;
        }

        internal static UserCredential GetCredential()
        {
            UserCredential credential;

            using (var stream =
                new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            return credential;
        }
    }
}
